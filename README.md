# java-data-rotate

Java project for a coding exercise. The goal of the project is to rotate the data in a map structure
so that the values become the keys and the keys become the values.

### Problem description

The original data consists of a mapping from a day of the month number to temperature measurements
that were collected from some weather stations. Note in the following example that each day may have
a different number of measurements, and it's possible a day may be skipped.

    1 -> 89.0, 82.9, 85.8, 85.7, 85.0, 81.5, 82.4, 89.2, 84.9, 89.5
    2 -> 87.1, 84.7, 84.5, 90.0, 87.5, 87.0, 86.7, 81.0, 87.0, 87.7
    3 -> 84.4, 91.6, 80.4, 87.7, 87.4, 84.7, 82.0, 85.1
    4 -> 80.1, 84.3, 75.9, 75.2, 83.2, 73.2, 76.2
    5 -> 75.8, 72.2, 74.8, 75.2, 73.9, 78.1, 73.6, 79.2, 76.1, 74.1, 77.4
    7 -> 77.7, 71.1, 68.8, 78.7, 71.2, 67.7, 68.9, 67.8, 75.5, 75.4
    8 -> 94.2, 91.8, 92.3, 93.1, 85.9, 90.5, 89.9, 91.9, 86.8

The data needs to be transformed so we can easily determine on which days a temperature of 75 (for
example) was recorded. The transformed data should look like the following **incomplete** example.
Note that the temperature needs to be rounded to the nearest integer.

    81 -> 2
    83 -> 1, 4
    82 -> 1, 3
    84 -> 3, 4
    85 -> 1, 2, 3
    86 -> 1, 8
    87 -> 2, 3, 8
    89 -> 1
    90 -> 1, 2, 8
    etc.

### Instructions

1. Run the unit tests and observe that they all fail.
2. Implement the `transform` function in the `DataRotate` class so that all the tests pass. Make
   sure you do not modify the test code in any way. The tests start off as simple and become more
   complex, so you could start off getting the first one to pass and proceed from there.
