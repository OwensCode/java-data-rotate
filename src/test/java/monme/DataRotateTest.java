package monme;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.Collections.singletonList;
import static java.util.Collections.unmodifiableMap;
import static org.junit.Assert.assertEquals;

public class DataRotateTest {

    private static List<Float> floatList(Double... x) {
        return Collections.unmodifiableList(Arrays.stream(x).map(Double::floatValue).collect(Collectors.toList()));
    }

    @Test
    public void testTransformSingleValueOnSingleDay() {
        Map<Integer, List<Float>> original = new HashMap<>();
        original.put(1, singletonList(78.1f));
        original = unmodifiableMap(original);

        Map<Integer, List<Integer>> expected = new HashMap<>();
        expected.put(78, singletonList(1));
        expected = unmodifiableMap(expected);

        assertEquals(expected, DataRotate.transform(original));
    }

    @Test
    public void testTransformMultipleValuesOnSingleDay() {
        Map<Integer, List<Float>> original = new HashMap<>();
        original.put(1, Arrays.asList(89.0f, 82.9f, 85.8f));
        original = unmodifiableMap(original);

        Map<Integer, List<Integer>> expected = new HashMap<>();
        expected.put(83, singletonList(1));
        expected.put(86, singletonList(1));
        expected.put(89, singletonList(1));
        expected = unmodifiableMap(expected);

        assertEquals(expected, DataRotate.transform(original));
    }

    @Test
    public void testTransformMultipleValuesOnMultipleDays() {
        Map<Integer, List<Float>> original = new HashMap<>();
        original.put(1, floatList(89.0, 82.9, 85.8, 85.7, 85.0, 81.5, 82.4, 89.2, 84.9, 89.5));
        original.put(2, floatList(87.1, 84.7, 84.5, 90.0, 87.5, 87.0, 86.7, 81.0, 87.0, 87.7));
        original.put(3, floatList(84.4, 91.6, 80.4, 87.7, 87.4, 84.7, 82.0, 85.1));
        original.put(4, floatList(80.1, 84.3, 75.9, 75.2, 83.2, 73.2, 76.2));
        original.put(5, floatList(75.8, 72.2, 74.8, 75.2, 73.9, 78.1, 73.6, 79.2, 76.1, 74.1, 77.4));
        original.put(7, floatList(77.7, 71.1, 68.8, 78.7, 71.2, 67.7, 68.9, 67.8, 75.5, 75.4));
        original.put(8, floatList(94.2, 91.8, 92.3, 93.1, 85.9, 90.5, 89.9, 91.9, 86.8));
        original = unmodifiableMap(original);

        Map<Integer, List<Integer>> expected = new HashMap<>();
        expected.put(68, singletonList(7));
        expected.put(69, singletonList(7));
        expected.put(71, singletonList(7));
        expected.put(72, singletonList(5));
        expected.put(73, singletonList(4));
        expected.put(74, singletonList(5));
        expected.put(75, Arrays.asList(4, 5, 7));
        expected.put(76, Arrays.asList(4, 5, 7));
        expected.put(77, singletonList(5));
        expected.put(78, Arrays.asList(5, 7));
        expected.put(79, Arrays.asList(5, 7));
        expected.put(80, Arrays.asList(3, 4));
        expected.put(81, singletonList(2));
        expected.put(82, Arrays.asList(1, 3));
        expected.put(83, Arrays.asList(1, 4));
        expected.put(84, Arrays.asList(3, 4));
        expected.put(85, Arrays.asList(1, 2, 3));
        expected.put(86, Arrays.asList(1, 8));
        expected.put(87, Arrays.asList(2, 3, 8));
        expected.put(88, Arrays.asList(2, 3));
        expected.put(89, singletonList(1));
        expected.put(90, Arrays.asList(1, 2, 8));
        expected.put(91, singletonList(8));
        expected.put(92, Arrays.asList(3, 8));
        expected.put(93, singletonList(8));
        expected.put(94, singletonList(8));
        expected = unmodifiableMap(expected);

        assertEquals(expected, DataRotate.transform(original));
    }
}
